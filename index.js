/*
First we load the express modules in our application and saved it in a variable called express.
*/

const express = require('express');

//Create application with express
/*
This creates n application that uses express and stores it as app, appis now the server
*/
const app = express();

const port = 4000;
/*
Middle ware express json is a method from express which allow us to handle the streaming of data and automatically parse the incoming json from oue req body
app used to run a method or another function for our expressjs api
*/
app.use(express.json());

let users = [
	{
		username: "BMadrigal",
		email: "fateReader@gmail.com",
		password: "weDontTalkAboutMe"
	},
	{
		username: "LuisaMadrigal",
		email: "strongSis@gmail.com",
		password: "pressure"
	}
];

let items = [
	{
		name: "Roses",
		price: 170,
		isActive: true
	},
	{
		name: "Tulips",
		price: 250,
		isActive: true
	}
];

/*
Express has a methods to use as routes corresponding to http methods
app.get(<endpoint>, <function handliing req and res)
*/

app.get('/', (req, res) => {
	res.send('Hello from ExpressJS API!')
});
/*
once the route is access we can send a response with the use of res.send
combined writehead  and end
*/
app.get('/greeting', (req, res) => {
	res.send('Hello from Batch 169 - C.Chua!')
});
app.get('/users', (req, res) => {
	//res.send stringifies for you
	res.send(users);
});

app.post('/users', (req, res) => {
	console.log(req.body)
	let newUser = {
		username: req.body.username,
		email: req.body.email,
		password: req.body.password
	}
	users.push(newUser);
	console.log(users);
	res.send(users);
})

app.delete('/users', (req, res) =>{
	users.pop();
	consoloe.log(users);
	res.send(users)

})
//updating users route 
app.put('/users/:index', (req, res) => {
	console.log(req.body);
	//contain the updated data
	console.log(req.params);//id ?
	/*
	params contain the value in the url params 
	url params is captured by routes parameter (:parameterName) and saved as property req.params
	*/

	let index = parseInt(req.params.index);
	users[index].password = req.body.password;
	res.send(users[index]);
});

app.get('/users/getSingleUser/:index', (req, res) => {
	let index = parseInt(req.params.index);
	res.send(users[index]);
})

//mini activity

app.get('/items', (req, res) => {
	res.send(items);
});

app.post('/items', (req, res) => {
	console.log(req.body)
	let newItem = {
		name: req.body.name,
		price: req.body.price,
		isActive: req.body.isActive
	}
	items.push(newItem);
	console.log(items);
	res.send(items);
})

app.put('/items/:index', (req, res) => {
	console.log(req.body);
	console.log(req.params);//id ?
	let index = parseInt(req.params.index);
	items[index].price = req.body.price;
	res.send(items[index]);

})



// 	A C T I V I T Y   S E C T I O N 

app.get('/items/getSingleItem/:index', (req, res) => {
	let index = parseInt(req.params.index);
	res.send(items[index]);
})

app.put('/items/archive/:index', (req, res) => {
	console.log(req.body);
	console.log(req.params);//id ?
	let index = parseInt(req.params.index);
	items[index].isActive = req.body.isActive;
	res.send(items[index]);
})

app.put('/items/activate/:index', (req, res) => {
	console.log(req.body);
	console.log(req.params);//id ?
	let index = parseInt(req.params.index);
	items[index].isActive = req.body.isActive;
	res.send(items[index]);
})





app.listen(port, () => console.log (`Server is now running at port ${port}`))


